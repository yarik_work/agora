## AGORA

### Install:
```
npm install

```

### Run:
```
npm start
Go to localhost:8080

```

### Build:
```
npm run build

```


### About
```
Front-end: Javascript, React, Redux, ES6, Babel, Webpack

```
