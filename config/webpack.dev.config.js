const path = require('path');
const HtmlWebPackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const {EnvironmentPlugin} = require("webpack");

module.exports = {
  devtool: "source-map",
  context: path.join(__dirname, '../src'),
  entry: "./app/routes.tsx", 
  output: {
    path: path.join(__dirname, '../dist'),
    filename: '[name].bundle.js',
    publicPath: '/'
  },
  resolve: {
    extensions: ['.js', '.json', '.ts', '.tsx'],
  },
  module: {
    rules: [
      {
          test: /\.(ts|tsx)$/,
          loader: "awesome-typescript-loader"
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: "html-loader",
            options: { minimize: true }
          }
        ]
      },
      {
        test: /\.css$/,
        use: [MiniCssExtractPlugin.loader, "css-loader"]
      }
    ]
  },
  plugins: [
    new HtmlWebPackPlugin({
      template: "./index.html",
      filename: "./index.html"
    }),
    new MiniCssExtractPlugin({
      filename: "[name].css",
      chunkFilename: "[id].css"
    }),
    new EnvironmentPlugin({
      NODE_ENV: 'development'
    })
  ],
  devServer: {
    contentBase: path.join(__dirname, '../dist'),
    disableHostCheck: true,
    compress: true,
    historyApiFallback: true,
    noInfo: true
  },
};