const path = require('path');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const { EnvironmentPlugin } = require('webpack');

module.exports = {
  context: path.join(__dirname, '../src'),
  entry: './app/routes.tsx',
  output: {
    path: path.join(__dirname, '../dist'),
    filename: '[name].bundle.[chunkhash].js',
    publicPath: '/'
  },
  resolve: {
    extensions: ['.js', '.json', '.ts', '.tsx']
  },
  module: {
    rules: [
      {
        test: /\.(ts|tsx)$/,
        loader: 'awesome-typescript-loader'
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: 'html-loader',
            options: { minimize: true }
          }
        ]
      },
      {
        test: /\.css$/,
        use: [MiniCssExtractPlugin.loader, 'css-loader']
      }
    ]
  },
  plugins: [
    new HtmlWebPackPlugin({
      template: './index.html',
      filename: './index.html'
      //hash: true
    }),
    new MiniCssExtractPlugin({
      filename: '[name].[chunkhash].css',
      chunkFilename: '[id].css'
    }),
    new UglifyJsPlugin({
      test: /\.js($|\?)/i,
      cache: true,
      parallel: true
    }),
    new OptimizeCssAssetsPlugin({
      assetNameRegExp: /\.css$/g
    }),
    new EnvironmentPlugin({
      NODE_ENV: 'production'
    })
  ]
};
