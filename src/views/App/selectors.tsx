import { createSelector } from 'reselect';

import { Store } from '../../generic/types';

const selectLogin = (state: Store) => {
  return state.login;
};

export const selectIsRedirect = createSelector(
  selectLogin,
  (login) => login.isRedirect
);
