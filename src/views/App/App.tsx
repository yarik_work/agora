import * as React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Route, Switch } from 'react-router-dom';
import { createStructuredSelector } from 'reselect';

import { ConnectedRouter } from 'react-router-redux';

import Main from '../Main/Main';
import Login from '../Login/Login';
import AuthorizedRoute from '../../components/AuthorizedRoute';
import { selectIsRedirect } from './selectors';

interface MappedProps {
  history: any;
}

class App extends React.Component<MappedProps> {
  componentDidMount() {
    console.log(this.props);
  }

  shouldComponentUpdate(nextProps: any, nextState: any) {
    console.log('next Pops', nextProps);
    return true;
  }

  render() {
    const { history } = this.props;

    return (
      <ConnectedRouter history={history}>
        <Switch>
          <Route path="/login" component={Login} />
          <AuthorizedRoute path="/" component={Main} />
        </Switch>
      </ConnectedRouter>
    );
  }
}

export const mapStateToProps = () =>
  createStructuredSelector({
    isRedirect: selectIsRedirect
  } as any);

export const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators({}, dispatch);

export default connect<MappedProps, {}, {}>(
  mapStateToProps as any,
  mapDispatchToProps
)(App);
