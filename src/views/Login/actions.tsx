import { CHANGE_FIELD, REQUEST_FORM } from './constants';
import { HandleChangeFieldFn, HandleRequestFormFn } from './types';

export const changeField: HandleChangeFieldFn = (field, text) => {
  return {
    type: CHANGE_FIELD,
    payload: { field, text }
  };
};

export const requestForm: HandleRequestFormFn = (url) => {
  return {
    type: REQUEST_FORM,
    payload: {
      request: true,
      url
    }
  };
};
