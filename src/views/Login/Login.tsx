import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Form, Icon, Input, Button, Row, Col, Layout, Card, Alert } from 'antd';

import { changeField, requestForm } from './actions';
import { HandleChangeFieldFn, HandleRequestFormFn, LoginState } from './types';
import {
  selectUserName,
  selectPassword,
  selectOtp,
  selectErrorMsg,
  selectIsBtnLoading
} from './selectors';

import utils from '../../utils';

import './login.css';

const { getToken } = utils;

const FormItem = Form.Item;

interface MappedProps {
  username: string;
  password: string;
  otp: string;
  errorMsg: string;
  isLoading: boolean;
  isRedirect: boolean;
  form: any;
  history: any;
}

interface MappedActions {
  changeField: HandleChangeFieldFn;
  requestForm: HandleRequestFormFn;
}

interface EventTarget {
  value: string;
  name: string;
}

interface Event {
  target: EventTarget;
}

type Props = MappedActions & MappedProps;

class Login extends Component<Props> {
  handleSubmit = (e: any) => {
    e.preventDefault();
    this.props.form.validateFields((error: any) => {
      if (!error) {
        this.props.requestForm('api/login');
      }
    });
  };

  handleChangeField = (e: Event) => {
    this.props.changeField(e.target.name, e.target.value);
  };

  componentDidMount() {
    if (getToken()) {
      this.props.history.push('/');
    }
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const { errorMsg, isLoading } = this.props;
    const isError = errorMsg ? (
      <Alert message={errorMsg} className="login__error" type="error" />
    ) : null;

    return (
      <div className="login__cont ">
        <Layout className="login__cont-inner">
          <Row>
            <Col xs={{ span: 10, offset: 7 }} lg={{ span: 6, offset: 9 }}>
              <Card className="login__card">
                <Form
                  layout="vertical"
                  onSubmit={this.handleSubmit}
                  className="login-form"
                >
                  {isError}
                  <FormItem>
                    {getFieldDecorator('username', {
                      rules: [
                        {
                          required: true,
                          message: 'Please input username!'
                        }
                      ]
                    })(
                      <Input
                        onChange={this.handleChangeField}
                        name="username"
                        prefix={
                          <Icon
                            type="user"
                            style={{ color: 'rgba(0,0,0,.25)' }}
                          />
                        }
                        placeholder="Username"
                      />
                    )}
                  </FormItem>
                  <FormItem>
                    {getFieldDecorator('password', {
                      rules: [
                        {
                          required: true,
                          message: 'Please input password!'
                        }
                      ]
                    })(
                      <Input
                        onChange={this.handleChangeField}
                        name="password"
                        prefix={
                          <Icon
                            type="lock"
                            style={{ color: 'rgba(0,0,0,.25)' }}
                          />
                        }
                        type="password"
                        placeholder="Password"
                      />
                    )}
                  </FormItem>
                  <FormItem>
                    {getFieldDecorator('otp', {
                      rules: [{ required: false }]
                    })(
                      <Input
                        onChange={this.handleChangeField}
                        name="otp"
                        prefix={
                          <Icon
                            type="key"
                            style={{ color: 'rgba(0,0,0,.25)' }}
                          />
                        }
                        placeholder="OTP"
                      />
                    )}
                  </FormItem>
                  <Button
                    loading={isLoading}
                    type="primary"
                    htmlType="submit"
                    className="login-form-button"
                  >
                    Log in
                  </Button>
                </Form>
              </Card>
            </Col>
          </Row>
        </Layout>
      </div>
    );
  }
}

export const mapStateToProps = (state: LoginState) =>
  createStructuredSelector({
    login: selectUserName,
    password: selectPassword,
    otp: selectOtp,
    errorMsg: selectErrorMsg,
    isLoading: selectIsBtnLoading
  } as any);

export const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators(
    {
      changeField,
      requestForm
    },
    dispatch
  );

const LoginForm = Form.create()(Login);

export default connect<MappedProps, MappedActions, {}>(
  mapStateToProps as any,
  mapDispatchToProps
)(LoginForm);
