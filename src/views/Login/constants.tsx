const moduleName = 'login';

export const CHANGE_FIELD = `${moduleName}/CHANGE_FIELD`;
export const REQUEST_FORM = `${moduleName}/REQUEST_FORM`;
export const REQUEST_FORM_SUCCESS = `${moduleName}/REQUEST_FORM:SUCCESS`;
export const REQUEST_FORM_FAILED = `${moduleName}/REQUEST_FORM:FAILED`;
