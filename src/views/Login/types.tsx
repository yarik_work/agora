import { TypedAction } from '../../generic/types';

export interface LoginState {
  username: string;
  password: string;
  otp: string;
  errorMsg: string;
  isLoading: boolean;
  isRedirect: boolean;
}

export interface HandleChangeFieldPayload {
  field: string;
  text: string;
}

export interface HandleRequestFormPayload {
  request: boolean;
  url: string;
}

export interface HandleErrorPayload {
  errorMsg: string;
}

export type HandleChangeFieldFn = (
  field: string,
  text: string
) => TypedAction<HandleChangeFieldPayload>;

export type HandleRequestFormFn = (
  url: string
) => TypedAction<HandleRequestFormPayload>;
