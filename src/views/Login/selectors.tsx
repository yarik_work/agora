import { createSelector } from 'reselect';

import { Store } from '../../generic/types';

const selectLogin = (state: Store) => {
  return state.login;
};

export const selectUserName = createSelector(selectLogin, (login) => {
  return login.username;
});

export const selectPassword = createSelector(selectLogin, (login) => {
  return login.password;
});

export const selectOtp = createSelector(selectLogin, (login) => login.otp);

export const selectErrorMsg = createSelector(
  selectLogin,
  (login) => login.errorMsg
);

export const selectIsBtnLoading = createSelector(
  selectLogin,
  (login) => login.isLoading
);
