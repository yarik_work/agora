jest.mock('../actions');
import { changeField, requestForm } from '../actions';

import { mapDispatchToProps } from '../Login';
import * as redux from 'redux';

describe('Login component tests', () => {
  describe('#mapDispatchToProps', () => {
    it('should map action creators to props', () => {
      redux.bindActionCreators = jest.fn();
      const dispatch = jest.fn();
      const actionCreators = {
        changeField,
        requestForm
      };

      mapDispatchToProps(dispatch);

      expect(redux.bindActionCreators).toHaveBeenCalledWith(
        actionCreators,
        dispatch
      );
    });
  });
});
