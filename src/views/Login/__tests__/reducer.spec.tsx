import loginReducer from '../reducer';

describe('Login reducer', () => {
  it('should init with initial state', () => {
    const initialState = {
      username: '',
      password: '',
      otp: '',
      errorMsg: '',
      isLoading: false,
      isRedirect: false
    };
    expect(loginReducer(undefined, {} as any)).toEqual(initialState);
  });
});
