import {
  CHANGE_FIELD,
  REQUEST_FORM,
  REQUEST_FORM_SUCCESS,
  REQUEST_FORM_FAILED
} from './constants';

import {
  LoginState,
  HandleChangeFieldPayload,
  HandleErrorPayload
} from './types';
import { TypedAction } from '../../generic/types';

const loginReducer = (
  state: LoginState = {
    username: '',
    password: '',
    otp: '',
    errorMsg: '',
    isLoading: false,
    isRedirect: false
  },
  action: any
) => {
  switch (action.type) {
    case CHANGE_FIELD: {
      const { payload } = action as TypedAction<HandleChangeFieldPayload>;
      const { field, text } = payload as HandleChangeFieldPayload;

      return {
        ...state,
        [field]: text
      };
    }

    case REQUEST_FORM: {
      return {
        ...state,
        isLoading: true
      };
    }

    case REQUEST_FORM_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        isRedirect: true
      };
    }

    case REQUEST_FORM_FAILED: {
      const { payload } = action as TypedAction<HandleErrorPayload>;
      const { errorMsg } = payload as HandleErrorPayload;

      return {
        ...state,
        errorMsg,
        isLoading: false
      };
    }
  }
  return state;
};

export default loginReducer;
