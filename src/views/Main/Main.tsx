import * as React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

interface MappedProps {
  isLoading: boolean;
}

export class Main extends React.Component<MappedProps> {
  render() {
    return <h1>MAIN PAGE</h1>;
  }
}

export const mapStateToProps = (state: any) => {
  return {
    isLoading: state.login.isLoading
  };
};

export const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators({}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Main);
