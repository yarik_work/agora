import setDefaultAuthHeader from './authorization/setDefaultAuthHeader';
import setToken from './authorization/setToken';
import getToken from './authorization/getToken';
import deleteToken from './authorization/deleteToken';

export default {
  setDefaultAuthHeader,
  setToken,
  getToken,
  deleteToken
};
