import localStorageKeys from './localStorageKeys';

const setToken = (token: any) => {
  localStorage.setItem(localStorageKeys.token, token);
};

export default setToken;
