import localStorageKeys from './localStorageKeys';

const deleteToken = () => {
  localStorage.removeItem(localStorageKeys.token);
};

export default deleteToken;
