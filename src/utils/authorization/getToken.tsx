import localStorageKeys from './localStorageKeys';

const getToken = () => {
  return localStorage.getItem(localStorageKeys.token);
};

export default getToken;
