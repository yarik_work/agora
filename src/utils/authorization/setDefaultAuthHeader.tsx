import axios from 'axios';

import getToken from './getToken';

const setDefaultAuthHeader = () => {
  axios.defaults.headers.common.Authorization = 'Bearer ' + getToken();
};

export default setDefaultAuthHeader;
