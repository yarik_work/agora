import * as React from 'react';
import {
  Route,
  Redirect,
  RouteComponentProps,
  RouteProps
} from 'react-router-dom';

import utils from '../utils/index';

const { getToken } = utils;
const AUTHENTICATED = getToken();

type RouteComponent =
  | React.StatelessComponent<RouteComponentProps<{}>>
  | React.ComponentClass<any>;

const AuthorizedRoute: React.StatelessComponent<RouteProps> = ({
  component,
  ...rest
}) => {
  const renderFn = (Component?: RouteComponent) => (props: RouteProps) => {
    if (!Component) {
      return null;
    }

    console.log(rest);

    if (AUTHENTICATED) {
      return <Component {...props} />;
    }

    return <Redirect to="/login" />;
  };

  return <Route {...rest} render={renderFn(component)} />;
};

export default AuthorizedRoute;
