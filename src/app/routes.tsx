import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import { routerMiddleware } from 'react-router-redux';
import thunk from 'redux-thunk';
import createHistory from 'history/createBrowserHistory';

import rootReducer from './reducer';
import loggingMiddleware from './middleware';

import App from '../views/App/App';

import 'antd/dist/antd.css';

const history = createHistory();

const initialState = {};
const enhancers = [];
const middleware = [thunk, routerMiddleware(history)];

declare global {
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION__: any;
  }
}

const devToolsExtension = window.__REDUX_DEVTOOLS_EXTENSION__;

if (
  typeof devToolsExtension === 'function' &&
  process.env.NODE_ENV !== 'production'
) {
  enhancers.push(devToolsExtension());
}

const composedEnhancers = compose(
  applyMiddleware(...middleware, loggingMiddleware),
  ...enhancers
);

const store = createStore(rootReducer, initialState, composedEnhancers);

render(
  <Provider store={store}>
    <App history={history} />
  </Provider>,
  document.getElementById('root')
);
