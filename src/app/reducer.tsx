import { combineReducers } from 'redux';

import loginReducer from '../views/Login/reducer';

const rootReducer = combineReducers({
  login: loginReducer
});

export default rootReducer;
