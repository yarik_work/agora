const loggingMiddleware = (store: any) => (next: any) => (action: any) => {
  if (action.payload.request) {
    const typeAction = action.type;
    next(action);
    setTimeout(() => {
      store.dispatch({
        type: `${typeAction}:SUCCESS`,
        payload: {
          errorMsg: 'sorry'
        }
      });
    }, 2000);
  } else {
    next(action);
  }
};

export default loggingMiddleware;
