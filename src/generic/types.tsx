import { LoginState } from '../views/Login/types';

export interface Store {
  login: LoginState;
}

export interface TypedAction<payload> {
  type: string;
  payload?: payload;
}

export interface TypedActionNoPayload {
  type: string;
}
